#include <stdio.h>

int main(void)
{
    int result;
    int numerator, denominator;

    // Dělení v pevné řadové čárce

    numerator = 54321;
    denominator = 1024;

    // čitatel násobím 100x protože chci počítat s přesností na dvě desetinná místa
    // ... aby se správně zaokrouhlilo, přičítám k čítateli půlku jmonovatele
    result = (100 * numerator + denominator / 2) / denominator;
    
    printf("%d,%02d\n", result / 100, result % 100);
    printf("%d,%02d\n", result / 100, result % 100);
    // 54321 / 1024 = 53.0478

    putchar('\n');

    printf("%d\n", result);
    printf("%7d\n", result);
    printf("%07d\n", result);

    printf("%7.2f\n", 54321./1024.);



}
